using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class playerdamage : MonoBehaviour
{
   public float hp = 100;
    public TMP_Text hpText;

       void Start()
        {
        hpText.text = ((int)hp).ToString(); 
        }

        public void FixedUpdate()
        {
            // print(hp);

            hpText.text = ((int)hp).ToString();
        }

     private void OnTriggerEnter(Collider collision)
     {

        if (collision.gameObject.tag == "DammagForPlayer")
        {
            hp -= 7;
        }
        if (collision.gameObject.tag == "HelsForPlayer")
        {
            hp += 48;
        }
    }
}