using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;
using Valve.VR.InteractionSystem;
public class gun : MonoBehaviour
{
    public SteamVR_Action_Boolean fireAction;
    public Transform barrePilot;
    public GameObject bellet;
    public GameObject puzzleFlash;
    public float shootingSpeed = 3;

    private Animator animator1;
    private Interactable interactable;

    void Start()
    {
        animator1 = GetComponent<Animator>();
        puzzleFlash.SetActive(false);
        interactable = GetComponent<Interactable>();
    }
    void Update()
    {

        if (interactable.attachedToHand != null)
        {

            SteamVR_Input_Sources source = interactable.attachedToHand.handType;
            if (fireAction[source].stateDown)
            {
                Fire();
            }



        }

    }

    public void Fire()
    {
        Rigidbody BulletRigidbody = Instantiate(bellet, barrePilot.position, barrePilot.rotation).GetComponent <Rigidbody>();
            
    }
}

